//
//  PieceButton.m
//  Chess
//
//  Created by Nicholas Haurus on 3/7/11.
//  Copyright 2011 Haurus Inc. All rights reserved.
//


#import "PieceButton.h"
#import "BoardView.h"

@implementation PieceButton

// Init the piece with it's correct image
-(id)initWithPiece:(NSString *)type andSize:(int) size  {
    squareSize = size;
	NSString *path = [NSString stringWithFormat:@"%@.png",type];
	UIImage *image = [UIImage imageNamed:path];
	[self setBackgroundImage:image forState:UIControlStateNormal];
	//[self addTarget:self action:@selector(draggedOut:withEvent: ) forControlEvents:UIControlEventTouchDragOutside | UIControlEventTouchDragInside];
	return self;
}

// Use this function to position the piece
-(void) setSquare:(unsigned int)squareValue {
	square = squareValue;
	float fx = (float) ( square % 8 ) * squareSize;
	float fy = (squareSize * 7) -floor( square / 8 ) * squareSize;
	[self setCenter:CGPointMake(fx,fy)];
}

// Makes the piece draggable
- (void)draggedOut:(UIControl *)c withEvent:(UIEvent *)ev {
	CGPoint cord = [[[ev allTouches] anyObject] locationInView:self];
	// Center of the piece plus the offset of the board view
	// Total iPad width 768, Board width 80*8= 640
	// 768-640=128
	// Half of the margin over and down
	// 128/2=64
	// Plus half of the piece size
	// 80/2 = 40
	// 40+64 = 104 net offset
	//cord.x -= squareSize;
	//cord.y -= squareSize;
	//c.center = cord;
} 

@end
