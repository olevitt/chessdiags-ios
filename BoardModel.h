//
//  BoardModel.h
//  Chess
//
//  Created by Nicholas Haurus on 3/7/11.
//  Copyright 2011 Haurus Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

extern NSString *const WHITE_KING;
extern NSString *const WHITE_QUEEN;
extern NSString *const WHITE_BISHOP;
extern NSString *const WHITE_KNIGHT;
extern NSString *const WHITE_ROOK;
extern NSString *const WHITE_PAWN;

extern NSString *const BLACK_KING;
extern NSString *const BLACK_QUEEN;
extern NSString *const BLACK_BISHOP;
extern NSString *const BLACK_KNIGHT;
extern NSString *const BLACK_ROOK;
extern NSString *const BLACK_PAWN;

extern NSString *const EMPTYSQUARE;

@interface BoardModel : NSObject {
    NSMutableArray *pieces;
    bool toPlay;
}

@property (nonatomic, retain) NSMutableArray *pieces;
@property (nonatomic) bool toPlay;
@property (nonatomic) int enPassant;
@property (nonatomic) NSString *roques;

-(void) clear;
-(void) reset;
-(void) print;
-(BOOL) isMate;
-(void) loadFEN:(NSString*) fen;
-(BOOL) playMove:(NSInteger) from to:(NSInteger) to;
-(BOOL) isDeplacable:(NSInteger) from;
-(NSString*) fenString;
-(void) chargerRoques:(NSString*) roques;

@end
