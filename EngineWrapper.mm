/*
  Stockfish, a UCI chess playing engine derived from Glaurung 2.1
  Copyright (C) 2004-2008 Tord Romstad (Glaurung author)
  Copyright (C) 2008-2014 Marco Costalba, Joona Kiiski, Tord Romstad

  Stockfish is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Stockfish is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#import "EngineWrapper.h"

#include "bitboard.h"
#include "evaluate.h"
#include "position.h"
#include "search.h"
#include "thread.h"
#include "tt.h"
#include "ucioption.h"
#include "types.h"

static EngineWrapper *singletonEngine;
@implementation EngineWrapper {
  NSThread *thread;
}

Position pos;

- (void)start
{
    thread = [[NSThread alloc] initWithTarget:self selector:@selector(start2) object:Nil];
    [thread start];
    //[self initEngine];
    [self performSelector:@selector(initEngine) onThread:thread withObject:Nil waitUntilDone:NO];
}

+ (EngineWrapper*) singleton {
    if (singletonEngine == nil) {
        NSLog(@"init");
        singletonEngine = [EngineWrapper new];
        [singletonEngine start];
    }
    return singletonEngine;
}

-(void) gameOver:(int)fin {
    [_engineListener gameOver:fin];
}

-(void)bestMove:(int)from to: (int) to {
    [_engineListener bestMoveFound:from to:to];
}

-(void)initEngine {
    std::cout << engine_info() << std::endl;
    
    UCI::init(Options);
    Bitboards::init();
    Position::init();
    Bitbases::init_kpk();
    Search::init();
    Pawns::init();
    Eval::init();
    Threads.init();
    TT.resize(Options["Hash"]);
}

-(BOOL) isValid:(NSString*) fen andMove:(NSString*) move {
    return UCI::is_valid(*new std::string([fen UTF8String]),*new std::string([move UTF8String]));
}

-(void) command:(NSString*) cmd{
    NSLog(@"Commande reçue %@",cmd);
    [self performSelector:@selector(syncCommand:) onThread:thread withObject:cmd waitUntilDone:NO];
}

-(void) syncCommand:(NSString*) cmd{
    UCI::command(*new std::string([cmd UTF8String]));
}

-(int) verifMate:(NSString *)fen {
    return UCI::verifMate(*new std::string([fen UTF8String]));
}

-(void)start2 {
    [[NSRunLoop currentRunLoop] addPort:[NSMachPort port] forMode:NSDefaultRunLoopMode];
    while(true)
    {
        [[NSRunLoop currentRunLoop] run];
    }
    //Threads.exit();
}
@end
