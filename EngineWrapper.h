//
//  EngineWrapper.m
//  Chessdiags
//
//  Created by olivier levitt on 12/06/14.
//  Copyright (c) 2014 Olivier levitt. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol EngineNotification
-(void)bestMoveFound:(NSInteger)from to: (NSInteger) to;
-(void)gameOver:(int) fin;

@end

@interface EngineWrapper : NSObject
-(void)start;
-(void)command:(NSString*) command;
-(void)syncCommand:(NSString*) command;
-(void)bestMove:(int)from to: (int) to;
-(void)gameOver:(int) fin;
-(int)verifMate:(NSString*) fen;
-(BOOL)isValid:(NSString*) move andMove: (NSString*) move;
+(EngineWrapper*)singleton;
@property (nonatomic, retain) IBOutlet id<EngineNotification> engineListener;
@end

