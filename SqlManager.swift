//
//  SqlManager.swift
//  Chessdiags
//
//  Created by olivier levitt on 13/06/14.
//  Copyright (c) 2014 Olivier levitt. All rights reserved.
//

import Foundation

class SqlManager {
    
    var database : FMDatabase
    
    init() {
        let documentsPath = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0] as String
        database = FMDatabase(path: documentsPath.stringByAppendingPathComponent("database6.sqlite"));
        database.open();
        database.executeUpdate("create table problemes(id int primary key, titre text, soustitre text, fen text, moves int,resolu boolean)",withArgumentsInArray: nil);
    }
    
    func insert(donnees: NSArray) {
        database.beginTransaction();
        database.executeUpdate("DELETE FROM problemes", withArgumentsInArray: nil);
        for diag in donnees  {
            var diagData = diag as NSDictionary;
            var toInsert: AnyObject[] = [diagData.valueForKey("id"),diagData.valueForKey("name"),diagData.valueForKey("description"),diagData.valueForKey("position"),diagData.valueForKey("moves"),false]
            database.executeUpdate("insert into problemes(id,titre,soustitre,fen,moves,resolu) VALUES(?,?,?,?,?,?)", withArgumentsInArray: toInsert);
        }
        database.commit();
    }
    
    func count() {
        var resultSet = database.executeQuery("SELECT COUNT(*) FROM problemes", withArgumentsInArray: nil);
        while(resultSet.next()) {
            var entier = resultSet.intForColumnIndex(0);
            println("\(entier)");
        }
    }
    
    func problemeResolu(identifiant : Integer) {
        var resultSet = database.executeUpdate("UPDATE problemes set resolu = true WHERE id = ?", withArgumentsInArray: ["\(identifiant)"]);
    }
    
    func getProblemes() -> Probleme[] {
        var resultSet = database.executeQuery("SELECT * FROM problemes ORDER BY moves,titre", withArgumentsInArray: nil);
        var problemes: Probleme[] = []
        while(resultSet.next()) {
            var pb = Probleme(name: resultSet.stringForColumn("titre"), description: resultSet.stringForColumn("soustitre"), position: resultSet.stringForColumn("fen"), moves: resultSet.intForColumn("moves"), identifiant: resultSet.intForColumn("id"), resolu: resultSet.boolForColumn("resolu"));
            problemes.append(pb);
        }
        return problemes
    }
    
    func close() {
        database.close();
    }
}