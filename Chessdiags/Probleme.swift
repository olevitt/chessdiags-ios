//
//  Probleme.swift
//  Chessdiags
//
//  Created by olivier levitt on 13/06/14.
//  Copyright (c) 2014 Olivier levitt. All rights reserved.
//

import Foundation

@objc class Probleme {
    
     var moves : Integer
    var name : String
    var description : String
    var position : String
   
    var identifiant : Integer
    var resolu : Bool
    
    init(name : String, description : String, position : String, moves : Integer, identifiant : Integer, resolu : Bool) {
        self.name = name;
        self.description = description;
        self.position = position;
        self.moves = moves;
        self.identifiant = identifiant;
        self.resolu = resolu;
    }
    
    class func newInstance(name : String, description : String, position : String, moves : Integer, identifiant : Integer,resolu : Bool) -> Probleme {
        var probleme = Probleme(name: name,description: description,position: position,moves: moves,identifiant: identifiant,resolu : resolu)
        return probleme
    }
    
}