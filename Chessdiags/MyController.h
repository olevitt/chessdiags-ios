//
//  MyController.h
//  Chessdiags
//
//  Created by olivier levitt on 17/06/14.
//  Copyright (c) 2014 Olivier levitt. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "Source.h"


@interface MyController : UIViewController < UITableViewDelegate, UITableViewDataSource> {
    IBOutlet UITableView *tableView;
    UIRefreshControl *refreshControl;
    
}
-(void) setSource:(Source *) source;
-(void) refreshUI;
@end
