//
//  ReposController.m
//  Chessdiags
//
//  Created by olivier levitt on 18/06/14.
//  Copyright (c) 2014 Olivier levitt. All rights reserved.
//
#import <Foundation/Foundation.h>
#import "ReposController.h"
#import "SqlManager.h"
#import "Source.h"
#import "MyController.h"
#import "ChessViewController.h"

@implementation ReposController

NSMutableArray *repos = NULL;


-(void) viewDidLoad {
    [self setTitle:@"Chessdiags"];
    repos = [NSMutableArray new];
}

-(void) viewDidAppear:(BOOL)animated {
    SqlManager *sql = [[SqlManager alloc] init];
    [repos removeAllObjects];
    [repos addObjectsFromArray:[sql getSources]];
    [sql close];
    [tableView reloadData];
    [EngineWrapper singleton]; //init asynchrone de l'engine
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    switch (section)
    {
        case 0:
            return repos.count;
        case 1:
            return 1;
        default:
            return 1;
    }
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    NSString *sectionName;
    switch (section)
    {
        case 0:
            sectionName = @"Repositories";
            break;
        case 1:
            sectionName = @"Regular game";
            break;
            // ...
        default:
            sectionName = @"";
            break;
    }
    return sectionName;
}

- (UITableViewCell *)tableView:(UITableView *)tableView2 cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView2 dequeueReusableCellWithIdentifier:@"cell"];
    if (cell == NULL) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"cell"];
    }

    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    if ([indexPath section] == 0) {
        Source *repo = (Source *) [repos objectAtIndex:[indexPath row]];
        cell.textLabel.text = [repo name];
        if (repo.nbtotal != 0) {
            cell.detailTextLabel.text = [NSString stringWithFormat:@"%d/%d (%.02f%%)",repo.nbresolu,repo.nbtotal,((float) repo.nbresolu / (float) repo.nbtotal * 100)];
        }
    }
    else {
        cell.textLabel.text = @"Game vs computer";
       cell.detailTextLabel.text = @"";
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

-(void) tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath {
    [self click:indexPath];
}

-(void) click:(NSIndexPath *) indexPath {
    if ([indexPath section] == 0) {
        Source *repo = (Source *) [repos objectAtIndex:[indexPath row]];
        UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Storyboard" bundle:nil];
        MyController *vc = (MyController *) [sb instantiateViewControllerWithIdentifier:@"liste"];
        vc.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
        [vc setSource:repo];
        [self.navigationController pushViewController:vc animated:TRUE];
        return;
    }
    if ([indexPath section] == 1) {
        Probleme *probleme = [Probleme new];
        [probleme setPosition:@"rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1"];
        [probleme setName:@""];
        [probleme setMoves:9999999];
        ChessViewController *controller = [ChessViewController new];
        [controller setProbleme:probleme];
        [[self navigationController] pushViewController:controller animated:TRUE];
        return;
    }
   
    //[self  presentViewController:vc animated:YES completion:NULL];
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self click:indexPath];
}

@end
