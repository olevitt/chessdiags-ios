//
//  MyController.swift
//  Chessdiags
//
//  Created by olivier levitt on 13/06/14.
//  Copyright (c) 2014 Olivier levitt. All rights reserved.
//

import UIKit

class MyController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet var tableView : UITableView
    @IBAction func refresh(AnyObject) {
         let manager = AFHTTPRequestOperationManager()
        manager.GET( "http://chessdiags.com/API/imaj.php?id=2",
            parameters: nil,
            success: { (operation: AFHTTPRequestOperation!,responseObject: AnyObject!) in
                var manager = SqlManager()
                
                var diags = responseObject.objectForKey("diags") as NSArray
                manager.count()
                manager.insert(diags)
                manager.count()
                manager.close()
                dispatch_async(dispatch_get_main_queue()) {
                    self.refreshUI()
                }
            },
            failure: { (operation: AFHTTPRequestOperation!,error: NSError!) in
                println("Error: " + error.localizedDescription)
            })
    }
    
    var problemes : Probleme[] = [];
    
    func refreshUI() {
        var SQLmanager = SqlManager()
        problemes = SQLmanager.getProblemes()
        SQLmanager.close()
        tableView.reloadData()
        var nbResolu = 0;
        var nbTotal = 0;
        for probleme in problemes {
            if (probleme.resolu) {
                nbResolu++;
            }
            nbTotal++;
        }
        navigationItem.title = "\(nbResolu) / \(nbTotal)";
    }
    
    //@IBOutlet var tableView : UITableView
    override func viewDidLoad()
    {
        super.viewDidLoad()
        refreshUI();
    }
    
    func tableView(tableView:UITableView!, numberOfRowsInSection section:Int)->Int
    {
        return problemes.count
    }
    
    func numberOfSectionsInTableView(tableView:UITableView!)->Int
    {
        return 1
    }
    
    
    func tableView(tableView: UITableView!, cellForRowAtIndexPath indexPath: NSIndexPath!) -> UITableViewCell
    {
        var cell : UITableViewCell! = tableView.dequeueReusableCellWithIdentifier("cell") as UITableViewCell
        if (cell == nil) {
            var cell = UITableViewCell(style:UITableViewCellStyle.Subtitle , reuseIdentifier: "cell")
        }
        
        cell.selectionStyle = UITableViewCellSelectionStyle.None;
        cell.accessoryType = UITableViewCellAccessoryType.DisclosureIndicator;
        cell.textLabel.text = self.problemes[indexPath.row].name;
        if (self.problemes[indexPath.row].resolu) {
            cell.imageView.image = UIImage(named: "star.png");
        }
        else {
            cell.imageView.image = UIImage(named: "wk.png");
        }
        var moves = self.problemes[indexPath.row].moves;
        cell.detailTextLabel.text = "Mat in \(moves)";
        
        return cell;
    }
    
    func tableView(tableView: UITableView!, didSelectRowAtIndexPath indexPath: NSIndexPath!) {
        var probleme = self.problemes[indexPath.row];
        var manager = SqlManager();
        manager.problemeResolu(probleme.identifiant);
        manager.close();
        var controller = ChessViewController();
        self.navigationController.pushViewController(controller, animated:true);
        controller.setProbleme(probleme);
    }
    
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
}
