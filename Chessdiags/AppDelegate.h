//
//  AppDelegate.h
//  Chessdiags
//
//  Created by olivier levitt on 17/06/14.
//  Copyright (c) 2014 Olivier levitt. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>
@property (strong, nonatomic) UIWindow *window;
@end
