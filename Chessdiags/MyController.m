//
//  MyController.m
//  Chessdiags
//
//  Created by olivier levitt on 17/06/14.
//  Copyright (c) 2014 Olivier levitt. All rights reserved.
//

#import "MyController.h"
#import "SqlManager.h"
#import "AFNetworking.h"
#import "ChessViewController.h"

@implementation MyController

NSMutableArray *problemes;
UIAlertView *loading;
Source *source;
UIImage *resolu;
UIImage *pasResolu;

-(void) viewDidLoad {
    [self setTitle:[source name]];
    resolu = [UIImage imageNamed:@"star.png"];
    pasResolu = [UIImage imageNamed:@"nostar.png"];
    problemes = [NSMutableArray new];
    UIRefreshControl *refresh = [[UIRefreshControl alloc] init];
    refresh.attributedTitle = [[NSAttributedString alloc] initWithString:@"Pull to Refresh"];
    [refresh addTarget:self action:@selector(pullToRefresh) forControlEvents:UIControlEventValueChanged];
    refreshControl = refresh;
    [self->tableView addSubview:refreshControl]; 
}

-(void) viewDidAppear:(BOOL)animated {
    [self refreshUI];
}

-(void) setSource:(Source *) source2 {
    source = source2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (problemes.count == 0) {
        return 1;
    }
    return problemes.count;
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(void) refreshUI {
    SqlManager *sql = [[SqlManager alloc] init];
    [problemes removeAllObjects];
    [problemes addObjectsFromArray:[sql getProblemes:source.identifiant]];
    [sql close];
    NSLog([NSString stringWithFormat:@"%d",[problemes count]]);
    [tableView reloadData];
}

-(void) pullToRefresh {
    [refreshControl endRefreshing];
    [self loadWebData];
}

-(void) loadWebData {
    loading = [[UIAlertView alloc] initWithTitle:@"Loading puzzles from the server\nPlease wait ..." message:nil delegate:self cancelButtonTitle:nil otherButtonTitles: nil] ;
    [loading show];
    
    UIActivityIndicatorView *indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    
    // Adjust the indicator so it is up a few pixels from the bottom of the alert
    indicator.center = CGPointMake(loading.bounds.size.width / 2, loading.bounds.size.height - 50);
    [indicator startAnimating];
    [loading addSubview:indicator];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager new];
    [manager GET:source.url parameters:nil success:^(AFHTTPRequestOperation *operation, id   responseObject) {
        SqlManager *sql = [[SqlManager alloc] init];
        NSArray *diags = [responseObject objectForKey:@"diags"];
        [sql insert:diags toSource:source.identifiant];
        [sql close];
        [self refreshUI];
        [loading dismissWithClickedButtonIndex:0 animated:YES];
    } failure:^(AFHTTPRequestOperation *operation, NSError *Error) {
        [loading dismissWithClickedButtonIndex:0 animated:YES];
        NSLog(@"%@",Error);
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                        message:@"Error while loading puzzles\nPlease verify your internet connection"
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        
        [alert show];
    }];

}

-(void) refresh:(id) sender {
    [self loadWebData];
}

- (UITableViewCell *)tableView:(UITableView *)tableView2 cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView2 dequeueReusableCellWithIdentifier:@"cell"];
    if (cell == NULL) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"cell"];
    }
 
    if (problemes.count == 0) {
        cell.textLabel.text = @"Looks like there is nothing here";
        cell.detailTextLabel.text = @"To begin, please pull to load the data";
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.accessoryType = UITableViewCellAccessoryNone;
        return cell;
    }
    Probleme *probleme = (Probleme *) [problemes objectAtIndex:indexPath.row];
    if ([probleme resolu]) {
        cell.imageView.image = resolu;
    }
    else {
        cell.imageView.image = pasResolu;
    }
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    cell.textLabel.text = [probleme name];
    cell.detailTextLabel.text = [NSString stringWithFormat:@"Mate in %d",[(Probleme *) [problemes objectAtIndex:indexPath.row] moves]];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

-(void) tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath {
    [self click:indexPath];
}

-(void) click:(NSIndexPath *) indexPath {
    if (problemes.count == 0) {
        return;
    }
    Probleme *probleme = (Probleme*) [problemes objectAtIndex:indexPath.row];
    ChessViewController *controller = [ChessViewController new];
    [controller setProbleme:probleme];
    [[self navigationController] pushViewController:controller animated:TRUE];
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self click:indexPath];
}


@end
