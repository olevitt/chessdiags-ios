//
//  Probleme.h
//  Chessdiags
//
//  Created by olivier levitt on 17/06/14.
//  Copyright (c) 2014 Olivier levitt. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface Probleme : NSObject {
    
}

@property(nonatomic,retain) NSString *name;
@property(nonatomic,retain) NSString *description;
@property(nonatomic,retain) NSString *position;
@property(nonatomic) NSInteger *identifiant;
@property(nonatomic) NSInteger *moves;
@property(nonatomic) NSInteger *source;
@property(nonatomic) BOOL *resolu;
@end
