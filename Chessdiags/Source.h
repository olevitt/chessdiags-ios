//
//  Source.h
//  Chessdiags
//
//  Created by olivier levitt on 18/06/14.
//  Copyright (c) 2014 Olivier levitt. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Source : NSObject {
    
}

@property(nonatomic) NSInteger identifiant;
@property(nonatomic) NSInteger nbtotal;
@property(nonatomic) NSInteger nbresolu;
@property(nonatomic) NSString *name;
@property(nonatomic) NSString *url;

@end
