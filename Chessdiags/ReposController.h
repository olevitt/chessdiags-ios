//
//  ReposController.h
//  Chessdiags
//
//  Created by olivier levitt on 18/06/14.
//  Copyright (c) 2014 Olivier levitt. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>


@interface ReposController : UIViewController < UITableViewDelegate, UITableViewDataSource> {
    IBOutlet UITableView *tableView;
}

@end
