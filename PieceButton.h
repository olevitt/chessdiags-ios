//
//  PieceButton.h
//  Chess
//
//  Created by Nicholas Haurus on 3/7/11.
//  Copyright 2011 Haurus Inc. All rights reserved.
//
#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@interface PieceButton : UIButton {
	unsigned int square;
    unsigned int squareSize;
}

-(id)initWithPiece:(NSString *)type andSize:(int) squareSize;
-(void)setSquare:(unsigned int)squareValue;

@end
