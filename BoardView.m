//
//  BoardView.m
//  Chess
//
//  Created by Nicholas Haurus on 3/7/11.
//  Copyright 2011 Haurus Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BoardView.h"
#import "PieceButton.h"
#import <UIKit/UIKit.h>
#import "EngineWrapper.h"


@implementation BoardView
 //const NSInteger GRIDSIZE = 16;
@synthesize model, squareSize, selectedSquare;

NSMutableDictionary *images;
UIImage *darksquareImage;

-(id) initWithBoardModel:(BoardModel *)model2 andFrame:(CGRect *)frame {
    
    self = [super initWithFrame:*frame];
    selectedSquare = -1;
    images = [NSMutableDictionary new];
    self.model = model2;
    CGFloat screenWidth = self.frame.size.width;
    NSLog(@"Ok %f",self.frame.size.width);
    CGFloat screenHeight = self.frame.size.height;
    NSLog(@"Ok %f",self.frame.size.height);
    squareSize = MIN(screenWidth,screenHeight) / 8;
    NSLog(@"SquareSize %f",squareSize);
    
    
    
    return self;
}



- (void) drawRect:(CGRect)rect {
    CGContextRef context = UIGraphicsGetCurrentContext();
    for (NSUInteger x = 0; x < 8; x++) {
        for (NSUInteger y = 0; y < 8; y++) {
            NSInteger square = [BoardView convertSquare:(x+y*8)];
            UIColor *color = (x % 2 == y % 2) ? [UIColor whiteColor] : [UIColor grayColor];
            if (x+y*8 == selectedSquare) {
                color = [UIColor redColor];
            }
            CGRect rect = CGRectMake(squareSize *x, squareSize*y + squareSize / 2, squareSize,squareSize);
            [color set];
            CGContextFillRect(context, rect);
            NSString *piece = [model.pieces objectAtIndex:square];
            UIImage *image = [images objectForKey:piece];
            if (image == nil && piece != nil) {
                image = [UIImage imageNamed:piece];
                if (image != nil) {
                    [images setObject:image forKey:piece];
                }
            }
            if (image != nil) {
                [image drawInRect:rect];
            }
        }
    }
}

+(int) convertSquare: (int) square {
    int x = square / 8;
    int y = square % 8;
    return (7-x) * 8 + y;
}





@end
