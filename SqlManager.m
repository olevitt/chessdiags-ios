//
//  SqlManager.m
//  Chessdiags
//
//  Created by olivier levitt on 17/06/14.
//  Copyright (c) 2014 Olivier levitt. All rights reserved.
//

#import "SqlManager.h"
#import "FMDatabase.h"
#import "Source.h"

@implementation SqlManager

FMDatabase *database;

-(void) close {
    [database close];
}

- (id)init {
    if ( self = [super init] ) {
        NSArray *docPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDir = [docPaths objectAtIndex:0];
        NSString *dbPath = [documentsDir   stringByAppendingPathComponent:@"database-49.sqlite"];
        database = [FMDatabase databaseWithPath:dbPath];
        [database open];
        FMResultSet *ps = [database executeQuery:@"PRAGMA USER_VERSION"];
        if ([ps next]) {
            int version = [ps intForColumn:@"user_version"];
            NSLog(@"DBVersion : %d",version);
            if (version == 0) {
                [database beginTransaction];
                [database executeUpdate:@"create table if not exists sources(id int primary key, name text,url text)"];
                [database executeUpdate:@"insert into sources(id,name,url) VALUES(?,?,?)" withArgumentsInArray:@[@1,@"Beginner repository",@"http://chessdiags.com/API/imaj.php?id=1"]];
                [database executeUpdate:@"insert into sources(id,name,url) VALUES(?,?,?)" withArgumentsInArray:@[@2,@"Wtharvey puzzles repository",@"http://chessdiags.com/API/imaj.php?id=2"]];
                [database executeUpdate:@"insert into sources(id,name,url) VALUES(?,?,?)" withArgumentsInArray:@[@3,@"Chessdiags.com public repository",@"http://chessdiags.com/API/imaj.php?id=3"]];
                [database executeUpdate:@"create table if not exists problemes(id int, source int,  titre text, soustitre text, fen text, moves int,resolu int,PRIMARY KEY (id,source))"];
                [database executeUpdate:[NSString stringWithFormat:@"PRAGMA USER_VERSION = %i", 1]];
                [database commit];
            }
        }
        return self;
    } else
        return nil;
}

-(void) insert:(NSArray *)donnees toSource:(NSInteger *)idSource {
    [database beginTransaction];
    FMResultSet *result = [database executeQuery:@"SELECT id FROM problemes WHERE source = ? AND resolu = 1" withArgumentsInArray:@[[NSString stringWithFormat:@"%d",idSource]]];
    NSMutableArray *resolus = [NSMutableArray new];
    while ([result next]) {
        [resolus addObject:[NSNumber numberWithInt:[result intForColumn:@"id"]]];
    }
    [database executeUpdate:@"DELETE FROM problemes WHERE source = ?" withArgumentsInArray:@[[NSString stringWithFormat:@"%d",idSource]]];
    for (NSDictionary* diagData in donnees) {
        NSArray *data =  @[[diagData valueForKey:@"id"],[NSString stringWithFormat:@"%d",idSource],[diagData valueForKey:@"name"],[diagData valueForKey:@"description"],[diagData valueForKey:@"position"],[diagData valueForKey:@"moves"],[NSString stringWithFormat:@"%d",0]];
        [database executeUpdate:@"insert into problemes(id,source,titre,soustitre,fen,moves,resolu) VALUES(?,?,?,?,?,?,?)" withArgumentsInArray:data];
    }
    [database executeUpdate:@"UPDATE problemes set resolu = 1 WHERE source = ? AND id in (?)" withArgumentsInArray:@[[NSString stringWithFormat:@"%d",idSource],[resolus componentsJoinedByString:@","]]];
    [database commit];
}

-(NSArray*) getSources {
    FMResultSet *result = [database executeQuery:@"SELECT sources.*,nbtotal,nbresolu FROM sources LEFT JOIN (SELECT source as idsource, COUNT(*) as nbtotal,SUM(resolu) as nbresolu FROM problemes GROUP BY source) jointure on sources.id = jointure.idsource   ORDER BY id"];
     NSMutableArray *array = [NSMutableArray new];
    while ([result next]) {
        Source *source = [Source new];
        source.name = [result stringForColumn:@"name"];
        source.identifiant = [result intForColumn:@"id"];
        source.url = [result stringForColumn:@"url"];
        source.nbtotal = [result intForColumn:@"nbtotal"];
        source.nbresolu = [result intForColumn:@"nbresolu"];
        [array addObject:source];
    }
    return array;
}

-(Probleme *) nextProblem:(NSInteger *)identifiant andSource:(NSInteger *) source {
    NSLog(@"%d %d",identifiant,source);
    FMResultSet *result = [database executeQuery:@"SELECT * FROM problemes WHERE id > ? AND source = ? ORDER BY id LIMIT 1" withArgumentsInArray : @[[NSString stringWithFormat:@"%d",identifiant],[NSString stringWithFormat:@"%d",source]]];
    if (![result next]) {
        return nil;
    }
    return [SqlManager initProbleme:result];
}

+(Probleme*) initProbleme:(FMResultSet*) result {
    Probleme *probleme = [Probleme new];
    probleme.name = [result stringForColumn:@"titre"];
    probleme.description = [result stringForColumn:@"soustitre"];
    probleme.position = [result stringForColumn:@"fen"];
    probleme.moves = [result intForColumn:@"moves"];
    probleme.identifiant = [result intForColumn:@"id"];
    probleme.source = [result intForColumn:@"source"];
    if ([result intForColumn:@"resolu"] == 1) {
        probleme.resolu = YES;
    }
    else {
        probleme.resolu = NO;
    }
    return probleme;
}

-(void) problemeResolu:(NSInteger *)identifiant andSource:(NSInteger *) source {
    NSLog(@"%d %d",identifiant,source);
    BOOL *ok = [database executeUpdate:@"UPDATE problemes set resolu = 1 WHERE id = ? AND source = ?" withArgumentsInArray : @[[NSString stringWithFormat:@"%d",identifiant],[NSString stringWithFormat:@"%d",source]]];
    NSLog(@"%d",ok);
}

-(NSArray*) getProblemes: (NSInteger *) source {
    FMResultSet *result = [database executeQuery:@"SELECT * FROM problemes WHERE source = ? ORDER BY moves,titre" withArgumentsInArray:@[[NSString stringWithFormat:@"%d",source]]];
    NSMutableArray *array = [NSMutableArray new];
    while ([result next]) {
        [array addObject:[SqlManager initProbleme:result]];
    }
    NSLog(@"Nb problemes : %d %d",[array count],source);
    return array;
}


@end
