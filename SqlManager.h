//
//  SqlManager.h
//  Chessdiags
//
//  Created by olivier levitt on 17/06/14.
//  Copyright (c) 2014 Olivier levitt. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Probleme.h"

@interface SqlManager : NSObject {
    
}
-(void) close;
-(void) insert:(NSArray *) donnees toSource:(NSInteger *) idSource;
-(void) problemeResolu:(NSInteger *) identifiant andSource:(NSInteger *) idSource;
-(NSArray*) getProblemes:(NSInteger *) sourceId;
-(NSArray*) getSources;
-(Probleme*) nextProblem:(NSInteger *) identifiant andSource:(NSInteger *) idSource;


@end
