//
//  BoardView.h
//  Chess
//
//  Created by Nicholas Haurus on 3/7/11.
//  Copyright 2011 Haurus Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BoardModel.h"
#import <UIKit/UIKit.h>
#import "EngineWrapper.h"

@interface BoardView : UIView  {
	UIView *squaresView;
	UIView *piecesView;

    
}
@property(nonatomic, assign) BoardModel *model;
- (id)initWithBoardModel:(BoardModel *)array andFrame:(CGRect *) frame;
@property(nonatomic, assign)     CGFloat squareSize;
@property(nonatomic, assign) int selectedSquare;
@end
