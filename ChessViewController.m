//
//  ChessViewController.m
//  Chess
//
//  Created by Nicholas Haurus on 3/7/11.
//  Copyright 2011 Haurus Inc. All rights reserved.
//

#import "ChessViewController.h"
#import "Probleme.h"
#import "SqlManager.h"

@implementation ChessViewController
Probleme *probleme;


// Create getters and setters
@synthesize boardView, boardModel;
int nbMoves;
BOOL gameOver = false;
NSInteger POPUPLOOSE = 1;
NSInteger POPUPWIN = 0;



// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
    UIBarButtonItem *flipButton = [[UIBarButtonItem alloc]
                                   initWithBarButtonSystemItem:UIBarButtonSystemItemRefresh target:self action:@selector(tryAgain)];
    //[flipButton set]
    //self.navigationItem.title =
    self.navigationItem.rightBarButtonItem = flipButton;
	// Add boardmodel
	boardModel = [[BoardModel alloc] init];
	[boardModel reset];
	[boardModel print];
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    CGFloat screenHeight = screenRect.size.height - self.navigationController.navigationBar.frame.size.height;
    CGRect rect = CGRectMake(0, self.navigationController.navigationBar.frame.size.height, screenWidth, screenHeight);
    boardView = [[BoardView alloc] initWithBoardModel:boardModel andFrame:&rect];
    UITapGestureRecognizer *singleFingerTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(handleSingleTap:)];
    [boardView addGestureRecognizer:singleFingerTap];
    [self loadProbleme];
    [EngineWrapper singleton].engineListener = self;
	[self.view addSubview:boardView];
}

-(void) updateTitle {
    if (nbMoves != -1 && nbMoves < 9999) {
        self.navigationItem.title = [NSString stringWithFormat:@"%d moves left",nbMoves];
    }
    else {
        self.navigationItem.title = @"";
    }
}

//The event handling method
- (void)handleSingleTap:(UITapGestureRecognizer *)recognizer {
    if (gameOver) {
        return;
    }
    int squareSize = boardView.squareSize;
    CGPoint location = [recognizer locationInView:boardView];
    int touchedSquare = ((int) (location.x / squareSize)) + 8 * ((int) ((location.y - squareSize / 2)  / boardView.squareSize));
    if (touchedSquare > 63) {
        return;
    }
    
    //NSString *piece = [model.pieces objectAtIndex:[BoardView convertSquare:touchedSquare]];
    if (boardView.selectedSquare == touchedSquare) {
        boardView.selectedSquare = -1;
    }
    else if (boardView.selectedSquare != -1) {
        EngineWrapper *wrapper = [EngineWrapper singleton];
        NSMutableString *coup = [NSMutableString new];
        [coup appendString:[self squareToString:boardView.selectedSquare]];
        [coup appendString:[self squareToString:touchedSquare]];
        BOOL legal = [wrapper isValid:[boardModel fenString] andMove:coup];
        if (legal) {
            [boardModel playMove:[ChessViewController convertSquare:boardView.selectedSquare] to:[ChessViewController convertSquare:touchedSquare]];
            nbMoves--;
            [self updateTitle];
            boardView.selectedSquare = -1;
            int state = [wrapper verifMate:[boardModel fenString]];
            if (state == -1) {
                [wrapper command:[NSString stringWithFormat:@"position fen %@",[boardModel fenString]]];
                [wrapper command:@"go"];
            }
            else {
                [self gameOver:state];
            }
        }
    }
    else if ([boardModel isDeplacable:[ChessViewController convertSquare:touchedSquare]]){
       boardView.selectedSquare = touchedSquare;
    }
    
    [boardView setNeedsDisplay];
}


-(void)bestMoveFound:(NSInteger)from to:(NSInteger) to {
    NSLog(@"Best move : %d %d",from, to);
    if (from == 0 && to == 0) {

    }
    else {
        //[NSThread sleepForTimeInterval:.5];
        EngineWrapper *wrapper = [EngineWrapper singleton];
        [boardModel playMove:from to:to];
        int state = [wrapper verifMate:[boardModel fenString]];
        if (state != -1) {
            [self gameOver:state];
        }
        else if (nbMoves <= 0) {
            gameOver = true;
            [self performSelectorOnMainThread:@selector(popupLoose) withObject:nil waitUntilDone:NO];
        }
    }
    [self performSelectorOnMainThread:@selector(refreshBoard) withObject:nil waitUntilDone:NO];
}

-(void) refreshBoard {
    [boardView setNeedsDisplay];
}

-(void) gameOver:(int)fin {
    gameOver = true;
    if (fin == 1) {
        [self performSelectorOnMainThread:@selector(popupLoose) withObject:nil waitUntilDone:NO];
    }
    else {
        [self performSelectorOnMainThread:@selector(popupWin) withObject:nil waitUntilDone:NO];
    }
}

-(void) popupLoose {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Fail :("
                                                    message:@"Try again ?"
                                                   delegate:self
                                          cancelButtonTitle:@"NO"
                                          otherButtonTitles:@"YES", nil];
    [alert setTag:POPUPLOOSE];
    [alert show];
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if ([alertView tag] == POPUPLOOSE) {
        if (buttonIndex == 1) {
            [self tryAgain];
        }
    }
    if ([alertView tag] == POPUPWIN) {
        if (buttonIndex == 1) {
            [self nextProblem];
            
        }
    }
}

-(void) nextProblem {
    SqlManager *manager = [SqlManager new];
    Probleme *problemeTemp = [manager nextProblem:[probleme identifiant] andSource:[probleme source]];
    [manager close];
    if (problemeTemp != nil) {
        probleme = problemeTemp;
        [self loadProbleme];
    }
}

-(void) popupWin {
    if ([probleme identifiant] == 0) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Game over"
                                                        message:@"The game is over"
                                                       delegate:self
                                              cancelButtonTitle:@"Ok"
                              otherButtonTitles:nil];
        [alert show];
    }
    else {
        SqlManager *manager = [SqlManager new];
        [manager problemeResolu:[probleme identifiant] andSource:[probleme source]];
        Probleme *problemeTemp = [manager nextProblem:[probleme identifiant] andSource:[probleme source]];
        [manager close];
        NSString *nextPb = @"Next problem";
        if (problemeTemp == nil) {
            nextPb = nil;
        }
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Congratz"
                                                        message:@"Well done !"
                                                       delegate:self
                                              cancelButtonTitle:@"Thanks"
                                              otherButtonTitles:nextPb,nil];
        [alert setTag:POPUPWIN];
        [alert show];

    }
}

+(int) convertSquare: (int) square {
    int x = square / 8;
    int y = square % 8;
    return (7-x) * 8 + y;
}

-(void) setProbleme:(id)probleme2{
    probleme = probleme2;
}

-(void) loadProbleme {
    gameOver = false;
    if(probleme != Nil) {
        nbMoves = [probleme moves];
        [self loadFen:probleme.position];
    }
    else {
        nbMoves = -1;
        [self loadFen:@"rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1"];
    }
    [boardView setNeedsDisplay];
    [self updateTitle];
}

-(void) tryAgain {
    [self loadProbleme];
}

-(void) loadFen: (NSString*) fen {
    [boardModel loadFEN:fen];
}

+(NSString*) stringToPiece: (NSString*) str {
    NSArray *myArray = @[
                         @[ @"P",WHITE_PAWN],
                         @[ @"p",BLACK_PAWN],
                         @[ @"R",WHITE_ROOK],
                         @[ @"r",BLACK_ROOK],
                         @[ @"N",WHITE_KNIGHT],
                         @[ @"n",BLACK_KNIGHT],
                         @[ @"B",WHITE_BISHOP],
                         @[ @"b",BLACK_BISHOP],
                         @[ @"Q",WHITE_QUEEN],
                         @[ @"q",BLACK_QUEEN],
                         @[ @"K",WHITE_KING],
                         @[ @"k",BLACK_KING],
                         ];
    for (NSArray *array in myArray) {
        if ([((NSString*)[array objectAtIndex:0]) isEqualToString:str]) {
            return [array objectAtIndex:1];
        }
    }
    return WHITE_QUEEN;
}

-(NSString*) squareToString: (NSInteger) square {
    NSMutableString *str = [NSMutableString new];
    [str appendString:[NSString stringWithFormat:@"%c",(char) (97+(square%8))]];
    [str appendString:[NSString stringWithFormat:@"%d",((63-square)/8)+1]];
    return str;
}

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];	
}


- (void)dealloc {

}

@end
