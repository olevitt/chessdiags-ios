//
//  BoardModel.m
//  Chess
//
//  Created by Nicholas Haurus on 3/7/11.
//  Copyright 2011 Haurus Inc. All rights reserved.
//

#import "BoardModel.h"
#import <Foundation/Foundation.h>


// Define your ids for the white pieces
NSString *const WHITE_KING =   @"wk";
NSString *const WHITE_QUEEN =  @"wq";
NSString *const WHITE_BISHOP = @"wb";
NSString *const WHITE_KNIGHT = @"wn";
NSString *const WHITE_ROOK =   @"wr";
NSString *const WHITE_PAWN =   @"wp";
// Define your ids for the black pieces
NSString *const BLACK_KING =   @"bk";
NSString *const BLACK_QUEEN =  @"bq";
NSString *const BLACK_BISHOP = @"bb";
NSString *const BLACK_KNIGHT = @"bn";
NSString *const BLACK_ROOK =   @"br";
NSString *const BLACK_PAWN =   @"bp";
// Define your id for the an empty space
NSString *const EMPTYSQUARE =   @"em";

@implementation BoardModel

@synthesize pieces;
@synthesize toPlay;
@synthesize enPassant;
@synthesize roques;

-(id) init {
	// This we hold which pieces should be on what square
	pieces = [[NSMutableArray alloc] initWithCapacity:64];
    roques = @"KQkq";
    toPlay = true;
    enPassant = -1;
	return self;
}

-(void) clear {
	// Removing any existing data
	[pieces removeAllObjects];
	// Setup the empty middle
	for (int i=0; i < 64; i++) {
		[pieces insertObject:EMPTYSQUARE atIndex:i];
	}
}

-(void) reset {
	[self clear];
	// Setup the white power pieces
	[pieces replaceObjectAtIndex:0 withObject:WHITE_ROOK];
	[pieces replaceObjectAtIndex:1 withObject:WHITE_KNIGHT];
	[pieces replaceObjectAtIndex:2 withObject:WHITE_BISHOP];
	[pieces replaceObjectAtIndex:3 withObject:WHITE_QUEEN];
	[pieces replaceObjectAtIndex:4 withObject:WHITE_KING];
	[pieces replaceObjectAtIndex:5 withObject:WHITE_BISHOP];
	[pieces replaceObjectAtIndex:6 withObject:WHITE_KNIGHT];
	[pieces replaceObjectAtIndex:7 withObject:WHITE_ROOK];
	// Setup the white pawns
	for (int i=8; i < 16; i++) {
		[pieces replaceObjectAtIndex:i withObject:WHITE_PAWN];
	}	
	// Setup the bacl pawns
	for (int i=48; i < 56; i++) {
		[pieces replaceObjectAtIndex:i withObject:BLACK_PAWN];
	}
	// Setup the black power pieces
	[pieces replaceObjectAtIndex:56 withObject:BLACK_ROOK];
	[pieces replaceObjectAtIndex:57 withObject:BLACK_KNIGHT];
	[pieces replaceObjectAtIndex:58 withObject:BLACK_BISHOP];
	[pieces replaceObjectAtIndex:59 withObject:BLACK_QUEEN];
	[pieces replaceObjectAtIndex:60 withObject:BLACK_KING];
	[pieces replaceObjectAtIndex:61 withObject:BLACK_BISHOP];
	[pieces replaceObjectAtIndex:62 withObject:BLACK_KNIGHT];
	[pieces replaceObjectAtIndex:63 withObject:BLACK_ROOK];
}

-(void) loadFEN:(NSString*) fen {
    int piece = 0;
    toPlay = true;
    int idFragment = 0;
    for (NSString* fragment in [fen componentsSeparatedByString:@" "]) {
        if (idFragment == 0) {
            for (NSString* s in [fragment componentsSeparatedByString:@"/"]) {
                for (int j = 0; j < s.length; j++) {
                    NSString* c = [s substringWithRange:NSMakeRange(j, 1)];
                    int valeur = [c integerValue];
                    if(valeur != 0)
                    {
                        for (int offset = 0; offset < valeur; offset++) {
                            [pieces replaceObjectAtIndex:[BoardModel convertSquare:piece] withObject:EMPTYSQUARE];
                            piece++;
                        }
                    }
                    else {
                        [pieces replaceObjectAtIndex:[BoardModel convertSquare:piece] withObject:[BoardModel stringToPiece:c]];
                        piece++;
                    }
                }
            }
        }
        if (idFragment == 1) {
            if (![[fragment substringWithRange:NSMakeRange(0, 1)] isEqualToString:@"w"]) {
                toPlay = false;
            }
            else {
                toPlay = true;
            }
        }
        if (idFragment == 2) {
            [self chargerRoques:fragment];
        }
        if (idFragment == 3) {
            
        }
        idFragment++;
    }
    if (idFragment < 2) {
        [self chargerRoques:@"KQkQ"];
    }
    [self majRoques];
}

-(BOOL) playMove:(NSInteger) from to: (NSInteger) to {
    NSString *piece1 = [pieces objectAtIndex:from];
    NSString *piece2 = [pieces objectAtIndex:to];
    if (to == enPassant && ([piece1 isEqualToString:BLACK_PAWN] || [piece1 isEqualToString:WHITE_PAWN])) {
        if ([piece1 isEqualToString:BLACK_PAWN]) {
            [pieces replaceObjectAtIndex:(to + 8) withObject:EMPTYSQUARE];
        }
        else {
            [pieces replaceObjectAtIndex:(to - 8) withObject:EMPTYSQUARE];
        }
    }
    if ([piece1 isEqualToString:WHITE_KING] || [piece1 isEqualToString:BLACK_KING]) {
        if (from == 4 && to == 6) {
             [pieces replaceObjectAtIndex:5 withObject:[pieces objectAtIndex:7]];
             [pieces replaceObjectAtIndex:7 withObject:EMPTYSQUARE];
        }
        if (from == 4 && to == 2) {
            [pieces replaceObjectAtIndex:3 withObject:[pieces objectAtIndex:0]];
            [pieces replaceObjectAtIndex:0 withObject:EMPTYSQUARE];
        }
        if (from == 60 && to == 62) {
            [pieces replaceObjectAtIndex:61 withObject:[pieces objectAtIndex:63]];
            [pieces replaceObjectAtIndex:63 withObject:EMPTYSQUARE];
        }
        if (from == 60 && to == 58) {
            [pieces replaceObjectAtIndex:59 withObject:[pieces objectAtIndex:56]];
            [pieces replaceObjectAtIndex:56 withObject:EMPTYSQUARE];
        }
    }
    [pieces replaceObjectAtIndex:to withObject:piece1];
    [pieces replaceObjectAtIndex:from withObject:EMPTYSQUARE];

    if (to >= 24 && to <= 31 && [piece1 isEqualToString:WHITE_PAWN] && (to - from) == 16) {
        enPassant = to-8;
    }
    else if (to >= 32 && to <= 39 && [piece1 isEqualToString:BLACK_PAWN] && (to - from) == -16) {
        enPassant = to+8;
    }
    else {
        enPassant = -1;
    }
    toPlay = !toPlay;
    [self majRoques];
    return YES;
}

-(BOOL) isMate {
    return YES;
}

-(BOOL) isDeplacable:(NSInteger) from {
    NSString *piece = [pieces objectAtIndex:from];
    if ([piece isEqualToString:EMPTYSQUARE]) {
        return false;
    }
    return [BoardModel isWhite:piece] == toPlay;
}

+(BOOL) isWhite:(NSString*) piece {
    return [piece hasPrefix:@"w"];
}

-(NSString*) fenString {
    NSMutableString *str = [NSMutableString new];
    int blanc = 0;
    for (int y=0; y < 8; y++) {
        for (int x=0; x < 8; x++) {
            NSString *piece = [pieces objectAtIndex:(7-y)*8+x];
            if ([piece isEqualToString:EMPTYSQUARE]) {
                blanc++;
            }
            else {
                if (blanc != 0) {
                    [str appendString:[NSString stringWithFormat:@"%i",blanc]];
                    blanc = 0;
                }
                [str appendString:[BoardModel pieceToString:piece]];
            }
        }
        if (blanc != 0) {
            [str appendString:[NSString stringWithFormat:@"%i",blanc]];
            blanc = 0;
        }
        if (y != 7) {
            [str appendString:@"/"];
        }
    }
    [str appendString:toPlay ? @" w" : @" b"];
    [str appendString:@" "];
    [str appendString:roques];
    if (enPassant != -1) {
        [str appendString:@" "];
        [str appendString:[self squareToString:(enPassant)]];
    }
    else {
        [str appendString:@" -"];
    }
    [str appendString:@" 0"];
    [str appendString:@" 1"];
    return str;
}

// A valuable tool for dumping what's in memory
// Example NSLog:
// --------- BOARD -----------
// | br bn bb bq bk bb bn br |
// | bp bp bp bp bp bp bp bp |
// | em em em em em em em em |
// | em em em em em em em em |
// | em em em em em em em em |
// | em em em em em em em em |
// | wp wp wp wp wp wp wp wp |
// | wr wn wb wq wk wb wn wr |
// ---------------------------

-(void) print {
	NSLog(@"--------- BOARD -----------");
	NSString * line;
	// Start at the top of the row first
	for( int i=7; i>=0; i--) {
		line = @"";
		// Simply move from left to right in that row
		for( int j=0; j<8; j++) {
			// Create a space between for readability
			line = [line stringByAppendingString:[NSString stringWithFormat:@" %@",[pieces objectAtIndex:i*8+j]]];	
		}
		NSLog(@"|%@ |",line);
	}
	NSLog(@"---------------------------");
}

+(NSString*) stringToPiece: (NSString*) str {
    NSArray *myArray = @[
                         @[ @"P",WHITE_PAWN],
                         @[ @"p",BLACK_PAWN],
                         @[ @"R",WHITE_ROOK],
                         @[ @"r",BLACK_ROOK],
                         @[ @"N",WHITE_KNIGHT],
                         @[ @"n",BLACK_KNIGHT],
                         @[ @"B",WHITE_BISHOP],
                         @[ @"b",BLACK_BISHOP],
                         @[ @"Q",WHITE_QUEEN],
                         @[ @"q",BLACK_QUEEN],
                         @[ @"K",WHITE_KING],
                         @[ @"k",BLACK_KING],
                         ];
    for (NSArray *array in myArray) {
        if ([((NSString*)[array objectAtIndex:0]) isEqualToString:str]) {
            return [array objectAtIndex:1];
        }
    }
    return WHITE_QUEEN;
}

-(void) majRoques {
    if (![[pieces objectAtIndex:4] isEqualToString:WHITE_KING]) {
        roques = [roques stringByReplacingOccurrencesOfString:@"Q" withString:@""];
        roques = [roques stringByReplacingOccurrencesOfString:@"K" withString:@""];
    }
    if (![[pieces objectAtIndex:60] isEqualToString:BLACK_KING]) {
        roques = [roques stringByReplacingOccurrencesOfString:@"q" withString:@""];
        roques = [roques stringByReplacingOccurrencesOfString:@"k" withString:@""];
    }
    if (![[pieces objectAtIndex:0] isEqualToString:WHITE_ROOK]) {
        roques = [roques stringByReplacingOccurrencesOfString:@"Q" withString:@""];
    }
    if (![[pieces objectAtIndex:7] isEqualToString:WHITE_ROOK]) {
        roques = [roques stringByReplacingOccurrencesOfString:@"K" withString:@""];
    }
    if (![[pieces objectAtIndex:56] isEqualToString:BLACK_ROOK]) {
        roques = [roques stringByReplacingOccurrencesOfString:@"q" withString:@""];
    }
    if (![[pieces objectAtIndex:63] isEqualToString:BLACK_ROOK]) {
        roques = [roques stringByReplacingOccurrencesOfString:@"k" withString:@""];
    }
}

-(void) chargerRoques:(NSString *)roques2 {
    if ([roques2 length] > 4) {
        roques = @"KQkq";
    }
    else if ([roques2 length] == 0) {
        roques = @"-";
    }
    else {
       self.roques = roques2;
    }
}

+(NSString*) pieceToString: (NSString*) piece {
    NSArray *myArray = @[
                         @[ @"P",WHITE_PAWN],
                         @[ @"p",BLACK_PAWN],
                         @[ @"R",WHITE_ROOK],
                         @[ @"r",BLACK_ROOK],
                         @[ @"N",WHITE_KNIGHT],
                         @[ @"n",BLACK_KNIGHT],
                         @[ @"B",WHITE_BISHOP],
                         @[ @"b",BLACK_BISHOP],
                         @[ @"Q",WHITE_QUEEN],
                         @[ @"q",BLACK_QUEEN],
                         @[ @"K",WHITE_KING],
                         @[ @"k",BLACK_KING],
                         @[ @"X", EMPTYSQUARE]
                         ];
    for (NSArray *array in myArray) {
        if ([((NSString*)[array objectAtIndex:1]) isEqualToString:piece]) {
            return [array objectAtIndex:0];
        }
    }
    return WHITE_QUEEN;
}

-(NSString*) squareToString: (NSInteger) square {
    NSMutableString *str = [NSMutableString new];
    [str appendString:[NSString stringWithFormat:@"%c",(char) (97+(square%8))]];
    [str appendString:[NSString stringWithFormat:@"%d",((square)/8)+1]];
    return str;
}

+(int) convertSquare: (int) square {
    int x = square / 8;
    int y = square % 8;
    return (7-x) * 8 + y;
}

@end
