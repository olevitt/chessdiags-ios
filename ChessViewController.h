//
//  ChessViewController.h
//  Chess
//
//  Created by Nicholas Haurus on 3/7/11.
//  Copyright 2011 Haurus Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BoardView.h"
#import "BoardModel.h"
#import "EngineWrapper.h"

@interface ChessViewController : UIViewController <EngineNotification,UIAlertViewDelegate> {
	BoardView *boardView;
	BoardModel *boardModel;
}

-(void) setProbleme:(id) probleme;

@property (nonatomic, retain) BoardView *boardView;
@property (nonatomic, retain) BoardModel *boardModel;


@end

